<?php
/**
 * Single Product Price --- CHILD THEME
 * - display price followed by shipping costs
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

?>
<p class="<?php echo esc_attr( apply_filters( 'woocommerce_product_price_class', 'price' ) );?>">
<?php echo $product->get_price_html();?>
    <span class='shipping_cost'><?php echo get_shipping_price('HR');?></span>
</p>
