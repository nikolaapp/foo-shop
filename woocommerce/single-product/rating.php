<?php
/**
 * Single Product Rating --- CHILD THEME
 * - display different review string for different review_count
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

global $product;

if ( ! wc_review_ratings_enabled() ) {
	return;
}

$rating_count = $product->get_rating_count();
$review_count = $product->get_review_count();
$average      = $product->get_average_rating();

if ( $rating_count > 0 ) : 
    $text_to_display = null;
    switch ($rating_count) {
        case 1:
            $text_to_display = "$rating_count ocjena kupca";
            break;   
        case 2:
        case 3:
        case 4:
            $text_to_display = "$rating_count ocjene kupaca";
            break;     
        default:
            $text_to_display = "$rating_count ocjena kupaca";
            break;
    }   
    ?>


	<div class="woocommerce-product-rating">
		<?php echo wc_get_rating_html( $average, $rating_count ); // WPCS: XSS ok. ?>
		<?php if ( comments_open() ) : ?>
			<?php //phpcs:disable ?>
                <!-- <a href="#reviews" class="woocommerce-review-link" rel="nofollow">(<?php printf( _n( '%s customer review', '%s customer reviews', $review_count, 'woocommerce' ), '<span class="count">' . esc_html( $review_count ) . '</span>' ); ?>)</a> -->
                <a href="#reviews" class="woocommerce-review-link" rel="nofollow"><?php echo $text_to_display;?></a>
			<?php // phpcs:enable ?>
		<?php endif ?>
	</div>

<?php endif; ?>
