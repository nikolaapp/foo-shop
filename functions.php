<?php

//custom function definitions
require get_theme_file_path('/inc/custom-functions.php');


/**
 * defining styles/scripts to use in child theme
 */
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
function my_theme_enqueue_styles() { 
    $parent_style = 'parent-style'; 
    /**
     * loading parent style
     * fixme - unnecessary??? - wp is loading parent style twice
     */
    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    
    //loading child style
     wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    ); 

    //loading custom js script
    wp_enqueue_script( 'custom-single-product', get_theme_file_uri('/js/custom-single-product.js'), NULL, '1.0', true); 
    
    wp_enqueue_style('font-awesome', '//cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/fontawesome.min.css');
      
}


/**
 * change cart text
 */
add_filter('woocommerce_product_single_add_to_cart_text', 'woo_custom_cart_button_text');
function woo_custom_cart_button_text() {
    return "Dodaj u košaricu";
}


/**
 * prices without decimal part
 */
add_filter( 'woocommerce_price_trim_zeros', '__return_true' );


/**
 * elements we do not want to be rendered
 * @hook 'init' --- these 'remove_action' will not work if not hooked to 'init'/or similar hook
 */
add_action( 'init', 'disable_elements' );
function disable_elements() {    
    //meta categories after cart
    remove_action( 'woocommerce_single_product_summary','shopper_template_single_meta',40 );
    //breadcrumb on top
    remove_action( 'shopper_content_top','woocommerce_breadcrumb',10);
}

/**
 * rearange rendering positions
 */
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 25 );

remove_action( 'woocommerce_simple_add_to_cart', 'woocommerce_simple_add_to_cart', 30 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_simple_add_to_cart', 30 );

    
/**
 * render promotion tags above the product content
 * - static: 
 * 'Velika potraznja!' 
 * 
 * - dynamic: 
 * @see get_5star_percent() - calculate percentage using DB
 * '50% kupaca je ocjenilo kupnju kao ODLIČNA!'
 *  */    
add_action( 'woocommerce_single_product_summary', 'promotion_tags_above', 12 );
function promotion_tags_above(){?>
    <div class="promo_above">
    
    <?php if (true) :  //todo - which condition to apply?>
        <span class="promo_potraznja">Velika potraznja!</span> 
    <?php endif;

    $ratings_5star = get_5star_percent();
    if ($ratings_5star >= 50) : //todo - set threshold to 50% ? ?>
        <br>
        <span class='promo_percent'><i class="fa fa-smile"></i> <?php echo get_5star_percent();?>% kupaca je ocjenilo kupnju kao ODLIČNA!</span>
    <?php endif; ?>
    </div> 
<?php }


/**
 * render promotion tag for delivery time, just below product content
 * @todo - does weekend extends delevery time
 * @todo - server time or CET?
 * @todo - hard-coded sunday as week start can be problem
 *  */   
add_action( 'woocommerce_single_product_summary', 'promotion_tags_delivery', 25 );
function promotion_tags_delivery(){
    $days_hr = ['nedelju','ponedeljak','utorak','sredu','četvrtak','petak','subotu'];
    $day_after_tomorrow = new DateTime('2day');//2day after this moment
    $index_of_day = date('w',strtotime($day_after_tomorrow->format('Y-m-d H:i:s')));
    $day_after_tomorrow_NAME = $days_hr[$index_of_day];
    ?>
    <p class="promo">Danas naručiš, u <span style="color:green;"><?php echo $day_after_tomorrow_NAME?> <?php echo $day_after_tomorrow->format('d.m.');?></span> već dobivaš!</p>  
<?php }


/**
 * render custom element for choosing quantity values 
 * @global $product
 * @see plugin - WooCommerce Tiered Price Table
 * - prices for single item are provided here, using $product
 * - prices for 2 or 3 items are provided on fronted, using plugin
 */
add_action( 'woocommerce_single_product_summary', 'custom_quantity', 25 );
function custom_quantity(){   
    global $product; 
    $price_reg = $product->get_regular_price();     
    $price_sale = $product->get_sale_price(); 
    ?>
    <div class="custom_quantity">
        <table>
            <thead>
                <tr>
                    <th></th>
                    <th class="top-odabir">TOP ODABIR</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td data-qty="1" data-price-reg="<?php echo $price_reg; ?>" data-price-sale="<?php echo $price_sale; ?>"class='active'><strong>1</strong> x <span><?php echo $price_sale; ?></span><small> kn/kom</small></td>
                    <td data-qty="2" data-price-reg="<?php echo 2*$price_reg; ?>" ><strong>2</strong> x <span></span><small> kn/kom</small></td>
                    <td data-qty="3" data-price-reg="<?php echo 3*$price_reg; ?>" ><strong>3</strong> x <span></span><small> kn/kom</small></td>
                </tr>
            </tbody>
        </table>
    </div>  
<?php }


/**
 * render promotion tag for number of visitors curently active on this page
 * @see plugin - Visitors Online by BestWebSoft
 */
add_action( 'woocommerce_single_product_summary', 'promotion_tags_visitors_online', 35);
function promotion_tags_visitors_online(){
    //echo "<pre>";global $product; print_r($product->rating_counts);  echo "</pre>";
    $visitors = total_users_online();
    switch ($visitors) {
        case '2':
        case '3':
        case '4':
            $visitors .= ' osobe gledaju';
            break;
        default:
            $visitors .= ' osoba gleda';
            break;
    }
    echo "<p class='total_viewers'>Trenutno $visitors ovaj proizvod!</p>";
}


/**
 * render 4 static promotion tags 
 */
add_action( 'woocommerce_after_single_product_summary', 'promotion_tags_bottom',5);
function promotion_tags_bottom(){?>    
    <div class='promo_below'>
            <div class='promo_item'>
                <i class="fa fa-lock"></i> 100% sigurna kupnja             
            </div>
            <div class='promo_item'>
                <i class="fa fa-smile"></i> 100% jamstvo zadovoljstva      
            </div>
            <div class='promo_item'>
                <i class="fa fa-truck"></i> Besplatna i brza dostava
            </div>
            <div class='promo_item'>
                <i class="fa fa-thumbs-up"></i> Placanje i pouzećem
            </div>
    </div>        
<?php }


    
    
















