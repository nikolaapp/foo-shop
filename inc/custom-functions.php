<?php
/**
 * Custom functions tool-box
 * ---this file is included in functions.php
 */

/**
 * Detection of user country code 
 * @todo what to do if IP is not available?
 * @see get_shipping_price()
 * @return String country code - HR,RS,US
 */
if ( ! function_exists( 'detect_user_country_code' ) ) {
    function detect_user_country_code(){

        $country_code = null;

        if ( class_exists( 'WC_Geolocation' ) ) {

            $ip_adress = WC_Geolocation::get_ip_address();
            $location = WC_Geolocation::geolocate_ip($ip_adress);
            
            if (empty ($location['country'])) {
                $ip_adress = WC_Geolocation::get_external_ip_address();
                $location = WC_Geolocation::geolocate_ip($ip_adress);
            } 
            
            $country_code = $location['country'];            
        } 
                
        return $country_code;            
    } 
}

/**
 * Get shipping price, according to user country code
 * @todo retrieve shipping costs from DB
 * @see detect_user_country_code()
 * @param String country_code
 * @return String containg price and appropriate text
 */
if ( ! function_exists( 'get_shipping_price' ) ) {
    function get_shipping_price($country_code = NULL){
        if (!$country_code) {
            $country_code = detect_user_country_code();
        }
        $shipping_price=null;

        switch ($country_code) {
            case 'HR':
            case 'RS':
            case 'SI':
            case 'BH':
                $shipping_price=' + besplatna dostava';
                break;            
            default:
                $shipping_price=' + troškovi dostave';
                break;
        }
                
        return $shipping_price;            
    } 
}

/**
 * get percent of 5star ratings form DB
 * @global $product
 * @return number||NULL percent or NULL if rating_counts are not available
 */
function get_5star_percent(){
    global $product; 
    $total_ratings = array_sum ($product->rating_counts);

    if ($total_ratings) {
        $ratings_5star = $product->rating_counts[5];
        return round(100*$ratings_5star/$total_ratings);
    } else {
        return null;
    }

}

/**
 * Display information about users online
 * modification of vstrsnln_info_display() --- Visitors Online by BestWebSoft plugin
 * @see vstrsnln_info_display()
 * @return number of curent users online
 */
if ( ! function_exists( 'total_users_online' ) ) {
	function total_users_online( $is_widget = false ) {
		global $wpdb, $vstrsnln_options;
		$vstrsnln_blog_id		= get_current_blog_id();
		$vstrsnln_content		= $vstrsnln_options['structure_pattern'];
		$vstrsnln_date_today	= date( 'Y-m-d', time() );

		/* get realtime info */
		$vstrsnln_type_user	= $wpdb->get_row( "
			SELECT COUNT( * ) AS 'guest',
				( SELECT COUNT( * ) FROM `" . $wpdb->base_prefix . "vstrsnln_detailing`
					WHERE `date_connection` = '" . $vstrsnln_date_today . "'
						AND `user_type` = 'bot'
						AND `time_off` IS NULL
						AND `blog_id` = '" . $vstrsnln_blog_id . "'
				) AS 'bot',
				( SELECT COUNT( * ) FROM `" . $wpdb->base_prefix . "vstrsnln_detailing`
					WHERE `date_connection` = '" . $vstrsnln_date_today . "'
						AND `user_type` = 'user'
						AND `time_off` IS NULL
						AND `blog_id` = '" . $vstrsnln_blog_id . "'
				) AS 'user'
			FROM `" . $wpdb->base_prefix . "vstrsnln_detailing`
			WHERE `date_connection` = '" . $vstrsnln_date_today . "'
			AND `user_type` = 'guest'
			AND `time_off` IS NULL
			AND `blog_id` = '" . $vstrsnln_blog_id . "'
			LIMIT 1"
		);

		$table_general = vstrsnln_get_table_general( $vstrsnln_blog_id );

		/* preparing data for placeholder replacing */
		$replacements = array (
			'total'		=> 0, /* total count will be calculated later */
			'users'		=> $vstrsnln_type_user->user,
			'guests'	=> $vstrsnln_type_user->guest,
			'bots'		=> $vstrsnln_type_user->bot,
		);

		/* get total "realtime visitors" count */
		if ( strpos( $vstrsnln_content, '{USERS}' ) ) {
			$replacements['total'] += $vstrsnln_type_user->user;
		}

		if ( strpos( $vstrsnln_content, '{GUESTS}' ) ) {
			$replacements['total'] += $vstrsnln_type_user->guest;
		}

		if ( strpos( $vstrsnln_content, '{BOTS}' ) ) {
			$replacements['total'] += $vstrsnln_type_user->bot;
		}

		/* add new replacements */
		if ( ! empty( $table_general ) ) {
			$max_replacements = array (
				'max-date'		=> $table_general->date_connection,
				'max-total'		=> $table_general->number_visits,
				'max-users'		=> $table_general->number_users,
				'max-guests'	=> $table_general->number_guests,
				'max-bots'		=> $table_general->number_bots
			);

			/* get total "max visitors" count */
			if ( ! strpos( $vstrsnln_content, '{MAX-USERS}' ) ) {
				$max_replacements['max-total'] -= $table_general->number_users;
			}

			if ( ! strpos( $vstrsnln_content, '{MAX-GUESTS}' ) ) {
				$max_replacements['max-total'] -= $table_general->number_guests;
			}

			if ( ! strpos( $vstrsnln_content, '{MAX-BOTS}' ) ) {
				$max_replacements['max-total'] -= $table_general->number_bots;
			}

		} else {
			/* max replacements = realtime replacements if $table_general is empty (at first day, you install the plugin) */
			$max_replacements = array(
				'max-date'		=> date( 'Y-m-d' ),
				'max-total'		=> $replacements['total'],
				'max-users'		=> $replacements['users'],
				'max-guests'	=> $replacements['guests'],
				'max-bots'		=> $replacements['bots'],
				'country'		=> __( 'No data', 'visitors-online' ),
				'browser'		=> __( 'No data', 'visitors-online' )
			);
		}

		$replacements = array_merge( $replacements, $max_replacements );

        return $replacements['total'];
	}
}
