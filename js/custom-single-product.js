jQuery("document").ready(function () {
    /**
     * get price rules
     * useing data-price-sale attribute to store values
     */
    var sale_price_rules = jQuery('[data-price-rules]').data('price-rules');
    var el = jQuery('div.custom_quantity');
    el.find('td[data-qty=2]').attr('data-price-sale',2*sale_price_rules[2]);
    el.find('td[data-qty=2] span').html(sale_price_rules[2]);
    el.find('td[data-qty=3]').attr('data-price-sale',3*sale_price_rules[3]);
    el.find('td[data-qty=3] span').html(sale_price_rules[3]);

    /**
     * manage click on custom quantity tags
     */
    jQuery("div.custom_quantity tbody").on("click", "td", set_quantity);
    function set_quantity(e) {
        //set visual style for selected element
        var quantity_elem = jQuery(e.target).closest("td");
        jQuery("div.custom_quantity td").removeClass("active");
        quantity_elem.addClass("active");

        //set proper input quantity number
        var quantity_num = parseInt(quantity_elem.data('qty'));
        jQuery('.single-product .cart .quantity input')[0].value = quantity_num;
        
        //set total sale price
        var currency = jQuery('.woocommerce-Price-currencySymbol').first().text();
        jQuery('.price del').text(quantity_elem.attr('data-price-reg')+currency);
        jQuery('.price ins').text(quantity_elem.attr('data-price-sale')+currency);
    }

});